#!/usr/bin/bash

add() {
  a=$1
  b=$2
  c=$a+$b
  echo "$c"
}

subtract() {
  a=$1
  b=$2
  c=$a-$b
  echo "$c"
}

divide() {
  a=$1
  b=$2
  c=$a/$b
  echo "$c"
}

multiply() {
  a=$1
  b=$2
  c=$a*$b
  echo "$c"
}
