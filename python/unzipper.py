#!/usr/bin/python3.11
import os
import subprocess

def unzipper():
    zip_dir = os.getcwd()
    dir_contents = os.listdir(zip_dir)

    zip_files = [file for file in dir_contents if file.endswith('.zip')]
    
    for zip_file in zip_files:
        zip_path = os.path.join(zip_dir, zip_file)
        subprocess.run(['unzip', zip_path])

if __name__ == "__main__":
    unzipper()

