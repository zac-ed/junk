#!/bin/bash

if [ ! -d ~/.todo ]; then
  mkdir ~/.todo
fi

cp todo todo.py ~/.local/bin
echo "make sure that ~/.local/bin is on \$PATH"
