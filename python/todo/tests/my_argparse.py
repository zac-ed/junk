import argparse

def main():
    todo = argparse.ArgumentParser(description='Simple argparse program')
    todo.add_argument('--arg1', help='Description of arg1')
    todo.add_argument('--arg2', help='Description of arg2')
    args = todo.parse_args()

    if args.arg1:
        print(f'Arg1 value: {args.arg1}')
    if args.arg2:
        print(f'Arg2 value: {args.arg2}')

if __name__ == "__main__":
    main()
