#!/usr/bin/python3.11

import sys
import os

def get_most_recent_file(directory):
    files = [os.path.join(directory, f) for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]
    files.sort(key=os.path.getmtime, reverse=True)
    return files[0] if files else None

def main():
    if sys.argv[1] == "new":
        
        title = sys.argv[2]
        items = " ".join(sys.argv[3:])
        items = items.replace(" ", "\n")
        
        directory = os.path.expanduser("~/.todo")
        file_path = os.path.join(directory, title)

        with open(file_path, "w") as file:
            file.write(items)
        print(f"list {title} created")

        pass

    elif sys.argv[1] == "listall":
        todo_dir = os.path.expanduser("~/.todo")
        ls_todo = os.listdir(todo_dir)
        ls_todo = " ".join(ls_todo)
        print(ls_todo)

    elif sys.argv[1] == "help" or sys.argv[1] == "-h" or sys.argv[1] == "--help":
        print("Usage: \n\ntodo new title items --creates a list with specified title containing any amount of items\n\ntodo listall --list all todo lists in ~/.todo\n\ntodo filename --outputs the list with specified filename\n\ntodo --outputs the contents of most recent todo list")

    elif len(sys.argv) == 1:
        most_recent_file = get_most_recent_file(os.path.expanduser("~/.todo"))
        if most_recent_file:
            with open(most_recent_file, "r") as file:
                content = file.read()
            print(content)
        else:
            print("No files found in ~/.todo")

    else:
        print("bad usage. try todo help")

if __name__ == "__main__":
    main()
