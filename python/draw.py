import matplotlib.pyplot as plt
import numpy as np

# Define the logistic curve function
def logistic_curve(x, L=1, k=1, x0=65):
    y = L / (1 + np.exp(-k * (x - x0)))
    return y

# Create a range of x values for plotting
x_values = np.linspace(40, 90, 100)  # Adjust range as needed

# Calculate corresponding y values using the logistic curve function
y_values = logistic_curve(x_values)

# Create the plot
plt.plot(x_values, y_values, label='Logistic Curve')

# Customize plot elements
plt.xlabel('Temperature (C)')
plt.ylabel('PWM Value')  # Assuming you're using this for PWM control
plt.title('Logistic Curve for PWM Control')
plt.grid(True)
plt.axhline(y=1, color='gray', linestyle='--', label='Asymptote')
plt.axvline(x=65, color='gray', linestyle='--', label='Center')
plt.legend()

# Show the plot
plt.show()
