import random

def main():

    print("Rock, paper, scissors")

    user_play = get_user_input()

    npc_play = get_npc_answer()

    game_result = (user_play, npc_play)

    table_of_results = {
        (0, 0): "Tie",
        (0, 1): "Loss",
        (0, 2): "Win",
        (1, 0): "Win",
        (1, 1): "Tie",
        (1, 2): "Loss",
        (2, 0): "Loss",
        (2, 1): "Win",
        (2, 2): "Tie"
    }

    result = table_of_results.get(game_result)

    print(result)


def get_user_input():
    
    while True:
        
        user_play = input("r, p, or s?")
        
        if user_play.startswith("r"):
            return 0
        elif user_play.startswith("p"):
            return 1
        elif user_play.startswith("s"):
            return 2
        else:
            print("Please choose rock, paper, or scissors.")


def get_npc_answer():
    
    npc_play = random.randint(0,2)
    
    if   npc_play == 0:
        print("rock")
    elif npc_play == 1:
        print("paper")
    else:
        print("scissors")
    return npc_play

if __name__ == "__main__":
    main()
