#!/usr/bin/python3.11

import os
import shutil

pwd = os.getcwd()

ls = os.listdir(pwd)

movers = []

file_types = {
    "Documents": [
        ".docx",
        ".pdf",
        ".txt",
        ".pptx",
        ".xlsx",
        ".doc",
        ".ppt",
        ".xls",
        ".rtf",
        ".odt"
    ],
    "Pictures": [
        ".jpg",
        ".png",
        ".jpeg",
        ".gif",
        ".bmp",
        ".svg",
        ".tiff",
        ".ico",
        ".webp",
        ".raw"
    ],
    "Videos": [
        ".mp4",
        ".mkv",
        ".avi",
        ".mov",
        ".wmv",
        ".flv",
        ".webm",
        ".mpeg",
        ".mpg",
        ".3gp"
    ],

}

document_extensions = file_types["Documents"]
picture_extensions = file_types["Pictures"]
video_extensions = file_types["Videos"]

for i in ls:
    if '.' in i:
        movers.append(i)
        for j in movers:
            file_ext = j.split('.')[1]
            if file_ext in document_extensions:
                destination = os.path.expanduser('~/coding/testdir')
                print(j, destination)
                shutil.move(destination, j)

        print(movers)
        #file_ext = i.split('.')[1]
        #shutil.move(i, testdir)        


checktype = type(ls)

print(checktype)
