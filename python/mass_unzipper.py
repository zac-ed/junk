#!/usr/bin/python

import os
import subprocess

def unzipper():
    pwd          = os.getcwd()
    dir_contents = os.listdir(pwd)

    target_files = [
        [file for file in dir_contents if file.endswith('.zip')],
        [file for file in dir_contents if file.endswith('.tar.gz')],
        [file for file in dir_contents if file.endswith('.tar.xz')],
        [file for file in dir_contents if file.endswith('.tar')],
        [file for file in dir_contents if file.endswith('.7z')],
        [file for file in dir_contents if file.endswith('.rar')]
    ]

    all_files = [file for sublist in target_files for file in sublist]

    for file_name in all_files:
        file_path = os.path.join(pwd, file_name)

        if file_name.endswith('.zip'):
            subprocess.run(['7z', 'e', file_path])
        elif file_name.endswith('.tar.gz'):
            subprocess.run(['tar', '-xvzf', file_path])
        elif file_name.endswith('.tar.xz') or file_name.endswith('.tar'):
            subprocess.run(['tar', '-xvf', file_path])
        elif file_name.endswith('.7z'):
            subprocess.run(['7z', 'e', file_path])
        elif file_name.endswith('.rar'):
            subprocess.run(['unrar', file_path])

unzipper()
