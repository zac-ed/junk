import random
import os

def random_wallpaper():
    wallp_dir = os.path.expanduser("~/wallpapers")
    dir_contents = []
    home = os.path.expanduser("~")
    for _, _, files in os.walk(wallp_dir, topdown=True):
        dir_contents.extend(files)

    if dir_contents:
        randy = random.randint(0, len(dir_contents) - 1)
        target_image = dir_contents[randy]
        wallpaper = os.path.join(wallp_dir, target_image)
        wallpaper = wallpaper.replace(home, "~")
        print(wallpaper)
        return wallpaper

    else:
        wallpaper=''
        return wallpaper

random_wallpaper()

