#!/usr/bin/python3.11

from gpiozero import PWMLED
from time import sleep

fan = PWMLED(4)
temp = []
while True: # vvv #
    ###  Get CPU temp reading   ### 
    ### parse using with open() ###
    ###   and string methods    ###
    sleep(5)
    reading = 35.5
    temp.append(reading)
    ### Current plan is use linear equation for CPU fan ###
    ### Points are (40, 0.4), (65, 1) ###
    if temp[0] >= 40.0:
        fan.value = 0.024*temp[0] - 0.56
        temp.pop()
    else:
        fan.value = 0
        temp.pop()
