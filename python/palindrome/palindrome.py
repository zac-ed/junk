#!/bin/python3.11

import sys

def main():
    
    user_string = sys.argv
    user_string = " ".join(user_string[1:])  
    user_string = user_string.strip()

    reversed_string = user_string[::-1]
    reversed_string = reversed_string.strip()

    if user_string == reversed_string:
        print("palindromic")
    else:
        print("not palindromic")

if __name__ == "__main__":
    main()
    
