#!/bin/bash

if [ ! -e ~/.local/bin/myscript.py ] || [ ! -e ~/.local/bin/myscriptwrapper ]; then
    cp *py *er ~/.local/bin
fi

echo "palindrome installed. make sure ~/.local/bin is on \$PATH"
