## My scripts, learning, and hobbying

    This repo contains stuff I've done for fun, 
    or to solve issues I have on the pc.
    
    Of note is a file sorter for decluttering
    + install script + script wrapper to call 
    it on cli easily

    Try the mass zipper util for when you've
    downloaded 50 archived gameboy games
    (I mean downloaded from a 
    'legitimate game cartridge')
